export { setupDomFromTemplate as setupDomFromTemplate };

// - wire up Mutation Observers to slot child elements in the appropriate slots of the
//   template document fragment
// - re-insert possibly existing old content of ce_root
function setupDomFromTemplate(ce_root, template_string){
  const old_inner = ce_root.innerHTML

  // replace content with template
  ce_root.innerHTML = template_string;

  // wire up MutationObservers that will put the content in the right slots
  const slots = Array.from(ce_root.querySelectorAll("slot"))
  new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      mutation.addedNodes.forEach(function(e){
        if (e.nodeType == 1 || e.nodeType == 3){
          const slot_name = (e.nodeType == 1 && e.getAttribute("slot")) || null,
                slot = slots.find(s => s.getAttribute("name") == slot_name);
          if (slot){
            slot.appendChild(e);
          }
        }
      })
    });
  }).observe(ce_root, {childList: true});

  // add back content that might have existed before the initializer ran
  // old_inner.each(function(){ce_root.append(this)})
  ce_root.insertAdjacentHTML("beforeend", old_inner)
}