require('jquery')
import $ from "jquery"
import { setupDomFromTemplate } from "./src/templating"
import { fireEvent } from "./src/events"

const WC = {}; // WebComponent name-space

// Global handle to this particular doc, so that we can resove references
WC.WebComponentsDoc = document;

WC.setup_dom_template = setupDomFromTemplate;
WC.fireEvent = fireEvent;

//
// DOM finding
//
WC.closest = function(src_element, selector){
  if (src_element.closest){ // modern browsers except IE understand "closest()"
    return src_element.closest(selector);
  } else {
    var run = src_element;
    while (run){
      if (run.matches(selector)) return run;
      else run = run.parentElement;
    }
    return null;
  }

};

// Return items of a nodelist that match a css selector expression
WC.select = function(nodeList, selector){
  return [].filter.call(nodeList, function(e){
    return (e.nodeType == 1 && e.matches(selector))
  });
};

WC.reject = function(nodeList, selector){
  return [].filter.call(nodeList, function(e){
    return (e.nodeType == 1 && !e.matches(selector))
  });
};

//
// DOM updating
//
function prepend(el, content){
  $(el).prepend(content)
};
WC.prepend = prepend;

function append(el, content){
  $(el).append(content);
};
WC.append = append;

function replace(el, content){
  $(el).replaceWith(content);
}
WC.replace = replace

function html(el, content){
  $(el).html(content); // use jquery instead of innerHTML so that JS gets executed
};
WC.html = html;

function before(el, content){
  $(el).before(content)
};
WC.before = before;

function after(el, content){
  $(el).after(content)
};
WC.after = after;

//
// Geometry
//
WC.document_offset = function(elem) {
  var offset = null;
  if ( elem ) {
      offset = {left: 0, top: 0};
      do {
          offset.top += elem.offsetTop;
          offset.left += elem.offsetLeft;
          elem = elem.offsetParent;
      } while ( elem );
  }
  return offset;
};

// Offset to first positioned (not static) ancestor element
WC.absolute_offset = function(elem) {
  var offset = null,
      run_el = elem;
  if ( run_el ) {
      offset = {left: 0, top: 0};
      do {
          offset.top += run_el.offsetTop;
          offset.left += run_el.offsetLeft;
          run_el = run_el.offsetParent;
      } while ( run_el && window.getComputedStyle(run_el).getPropertyValue("position") == 'static');
  }
  return offset;
};

// Scroll Offset to first positioned (not static) ancestor element
WC.absolute_scroll = function(elem) {
  var scroll = null,
      run_el = elem;
  if ( run_el ) {
      scroll = {x: 0, y: 0};
      do {
          scroll.y += run_el.scrollTop;
          scroll.x += run_el.scrollLeft;
          run_el = run_el.parentElement;
      } while ( run_el && window.getComputedStyle(run_el).getPropertyValue("position") == 'static');
  }
  return scroll;
};

//
//  Function reference lookup
//
// e.g. WC.function_named("console.log");
//  Note the function_name reference must already exist. If you just loaded a doc fragment ajaxy, the script
//     tag might not have been executed yet.
WC.function_named = function(function_name){
  if (function_name && function_name.length > 0){
    var parts = function_name.split("."),
        answer = window[parts[0]];
    for(var i=1; i<parts.length; i++){
      answer = answer[parts[i]];
    }
    return answer;
  }
};

//
//  AJAX
//
WC.$get = function(url, data, success, dataType){
  return $.get(url, data, success, dataType)
};

WC.$post = function(url, data, success, dataType){
  return $.post(url, data, success, dataType)
};

//
//  Array
//
WC.array_delete = function(array, element_to_delete){
  var i = array.indexOf(element_to_delete);
  if (i > -1) {
    array.splice(i, 1);
    return true;
  } else {
    return false;
  }
};

//
//  Form
//
WC.serialize_as_string = function(form){
  return $(form).serialize();
};

// turns a parameter string (e.g. from $form.serialize()) like name=Walt&type_id[]=12&type_id[]=3&filter[name]=gaga into
//   a plain object like {'name': 'Walt', 'type_id[]': [12,3], 'filter': {name:'gaga'}}.
//   Actually, the input string is url encoded and spaces are "+"
WC.serialize_as_object = function(parameter_string){
  return parameter_string.split("&").reduce(function(hsh, pair_string){
    if (pair_string == ''){
      return hsh;
    } else {
      var key_val = pair_string.split("=");
      return WC.railsy_update_property_hash(hsh,
                                            decodeURIComponent(key_val[0]),
                                            decodeURIComponent(key_val[1].replace(/\+/g, " ")));
    }
  }, {});
};

// this does not YET work with arrayed nested objects (any "[]" must be at the end of the parameter);
// If value is expected to be an array, but is not an array, value will be appended to a possibly existing array.
// also returns that hash
WC.railsy_update_property_hash = function(hash, key, value){
  var idx_of_open = key.indexOf("[");
  if (idx_of_open == -1){ // no [..] or []
    hash[key] = value;
  } else { // [..] or []

    var var_name = key.substr(0, idx_of_open);

    if (key[idx_of_open + 1] == "]") { // x[]
      if (key.length > idx_of_open + 2) throw("Can't yet decode variables where the array is at the end of the parameter name");
      if ($.isArray(value)){
        hash[var_name] = value;
      } else {
        if (!hash.hasOwnProperty(var_name)) hash[var_name] = [];
        hash[var_name].push(value);
      }
    } else { // x[....]
      if (!hash.hasOwnProperty(var_name)) hash[var_name] = {};
      var match_parts = key.match(/^\w*\[(\w*)](.*)$/);
      if (!match_parts) debugger;
      WC.railsy_update_property_hash(hash[var_name], match_parts[1] + match_parts[2], value);
    }
  }
  return hash;
}

export default WC;
export { prepend as prepend_html };
export { append as append_html };
export { replace as replace_html };
export { html as update_html };
export { after as html_after };
export { before as html_before };
